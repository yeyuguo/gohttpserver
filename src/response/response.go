/*
* go response object
 */

package response

import (
	"bytes"
	"compress/gzip"
	"fmt"
	"io/ioutil"
	"net"
	"os"
	"path/filepath"
	"reflect"
	"strconv"
	"strings"
	"time"

	"config"
	"logger"
	"request"
)

var procMeta = map[string]interface{}{
	"Connection": map[string]interface{}{
		"title": "Connection",
		"proc": func(res *Response, resStatusMeta *statusMeta) string {
			return "Keep-Alive"
		},
	},
	"CacheControl": map[string]interface{}{
		"title": "Cache-Control",
		"proc": func(res *Response, resStatusMeta *statusMeta) string {
			return "no-cache"
		},
	},
	"ContentType": map[string]interface{}{
		"title": "Content-Type",
		"proc": func(res *Response, resStatusMeta *statusMeta) string {
			contentTypeMeta := map[string]string{
				"html": "text/html",
				"js":   "application/javascript",
				"css":  "text/css",
				"png":  "image/png",
				"jpg":  "image/jpeg",
				"gif":  "image/gif",
			}
			fileSuffix := strings.Trim(filepath.Ext(resStatusMeta.FilePath), ".")
			contentType := contentTypeMeta[fileSuffix]
			return contentType
		},
	},
	"Expires": map[string]interface{}{
		"title": "Expires",
		"proc": func(res *Response, resStatusMeta *statusMeta) string {
			timeRange := time.Duration(time.Hour) * 48
			expireDate := time.Now().Add(timeRange)
			return getGMTTimeStr(expireDate)
		},
	},
	"LastModified": map[string]interface{}{
		"title": "Last-Modified",
		"proc": func(res *Response, resStatusMeta *statusMeta) string {
			if f, e := os.Stat(resStatusMeta.FilePath); e == nil {
				return getGMTTimeStr(f.ModTime())
			}
			return ""
		},
	},
	"Server": map[string]interface{}{
		"title": "Server",
		"proc": func(res *Response, resStatusMeta *statusMeta) string {
			return "GoHttpServer"
		},
	},
	"Date": map[string]interface{}{
		"title": "Date",
		"proc": func(res *Response, resStatusMeta *statusMeta) string {
			now := time.Now()
			return getGMTTimeStr(now)
		},
	},
	"ContentLength": map[string]interface{}{
		"title": "Content-Length",
		"proc": func(res *Response, resStatusMeta *statusMeta) string {
			if f, e := os.Stat(resStatusMeta.FilePath); e == nil {
				return fmt.Sprintf("%d", f.Size())
			}
			return "0"
		},
	},
	"TransferEncoding": map[string]interface{}{
		"title": "Transfer-Encoding",
		"proc": func(res *Response, resStatusMeta *statusMeta) string {
			return ""
		},
	},
	"ContentEncoding": map[string]interface{}{
		"title": "Content-Encoding",
		"proc": func(res *Response, resStatusMeta *statusMeta) string {
			return ""
		},
	},
}

// status meta struct
type statusMeta struct {
	StatusCode string
	HeaderKey  []string
	FilePath   string
	Content    string
}

// Go Response struct
type Response struct {
	Protocol         string
	StatusCode       string
	CacheControl     string
	Connection       string
	ContentType      string
	Expires          string
	LastModified     string
	Server           string
	Date             string
	ContentLength    string
	TransferEncoding string
	ContentEncoding  string

	Request *request.Request
}

// write response to conn
func GoResponse(r *request.Request) *Response {
	response := &Response{
		Protocol: "Http/1.1",
		Request:  r,
	}
	return response
}

// response status line
func (res *Response) responseStatusLine(resStatusMeta *statusMeta) string {
	res.StatusCode = resStatusMeta.StatusCode
	statusLine := fmt.Sprintf(
		"%s %s\n",
		res.Protocol,
		resStatusMeta.StatusCode,
	)
	return statusLine
}

// parse response header
func (res *Response) parseResHeader(resStatusMeta *statusMeta) {
	reflectRes := reflect.ValueOf(res).Elem()

	for _, headerKey := range resStatusMeta.HeaderKey {
		metaMap := procMeta[headerKey].(map[string]interface{})
		content := metaMap["proc"].(func(*Response, *statusMeta) string)(res, resStatusMeta)
		reflectRes.FieldByName(headerKey).SetString(content)
	}
}

// generate response header
func (res *Response) responseHeader(resStatusMeta *statusMeta) string {
	headerSlice := make([]string, 0, 20)
	reflectRes := reflect.ValueOf(res).Elem()

	for _, headerKey := range resStatusMeta.HeaderKey {
		metaMap := procMeta[headerKey].(map[string]interface{})
		title := metaMap["title"]
		content := reflectRes.FieldByName(headerKey)

		headerLine := fmt.Sprintf("%s: %s", title, content)
		headerSlice = append(headerSlice, headerLine)
	}
	header := strings.Join(headerSlice, "\n")
	return header
}

// generate response content
func (res *Response) responseContent(resStatusMeta *statusMeta) string {
	if contentByte, err := ioutil.ReadFile(resStatusMeta.FilePath); err == nil {
		var gzContent bytes.Buffer
		var resContent string

		gzWriter := gzip.NewWriter(&gzContent)
		_, err := gzWriter.Write(contentByte)

		if err != nil {
			logger.Logger.Warning(
				"compress file %s to gzip file failed",
				resStatusMeta.FilePath,
			)
			resContent = string(contentByte[:])
		} else {
			gzWriter.Flush()
			res.ContentEncoding = "gzip"
			res.ContentLength = strconv.Itoa(len(gzContent.Bytes()))
			resContent = gzContent.String()
		}

		defer gzWriter.Close()
		return resContent
	} else {
		res.ContentLength = strconv.Itoa(len(resStatusMeta.Content))
		return resStatusMeta.Content
	}
}

// get file path
func (res *Response) getFilePath() string {
	path := res.Request.PathInfo
	dir := config.SerConfig.StaticDir
	filename := path
	if path == "/" {
		filename = "index.html"
	}
	filePath := dir + filename
	return filePath
}

// get status meta
func (res *Response) getStatusMeta() *statusMeta {
	filePath := res.getFilePath()
	statusMetaMap := map[string]interface{}{
		"304": map[string]interface{}{
			"checkFunc": func() bool {
				if f, e := os.Stat(filePath); e == nil {
					modifiedSince := res.Request.IfModifiedSince
					modifiedTimeStr := getGMTTimeStr(f.ModTime())
					if modifiedSince == modifiedTimeStr {
						return true
					}
				}
				return false
			},
			"resStatusMeta": &statusMeta{
				StatusCode: "304 Not Modified",
				HeaderKey: []string{
					"Connection",
					"Server",
					"Date",
				},
				Content:  "",
				FilePath: filePath,
			},
		},

		"200": map[string]interface{}{
			"checkFunc": func() bool {
				if _, err := os.Stat(filePath); err == nil {
					return true
				}
				return false
			},
			"resStatusMeta": &statusMeta{
				StatusCode: "200 OK",
				HeaderKey: []string{
					"Connection",
					"ContentLength",
					"ContentType",
					"ContentEncoding",
					"Server",
					"Date",
					"LastModified",
				},
				Content:  "",
				FilePath: filePath,
			},
		},

		"404": map[string]interface{}{
			"checkFunc": func() bool {
				if _, err := os.Stat(filePath); os.IsNotExist(err) {
					return true
				}
				return false
			},
			"resStatusMeta": &statusMeta{
				StatusCode: "404 Not Found",
				HeaderKey: []string{
					"Connection",
					"ContentLength",
					"ContentType",
					"ContentEncoding",
					"Server",
					"Date",
				},
				Content:  "404 Not Found",
				FilePath: filePath,
			},
		},
	}

	for _, metaMap := range statusMetaMap {
		metaData := metaMap.(map[string]interface{})
		if result := metaData["checkFunc"].(func() bool)(); result == true {
			resStatusMeta := metaData["resStatusMeta"].(*statusMeta)
			return resStatusMeta
		}
	}
	return &statusMeta{}
}

// generate response
func (res *Response) generateResponse() string {
	resStatusMeta := res.getStatusMeta()

	resStatusLine := res.responseStatusLine(resStatusMeta)
	res.parseResHeader(resStatusMeta)
	resContent := res.responseContent(resStatusMeta)
	resHeader := res.responseHeader(resStatusMeta)

	response := fmt.Sprintf(
		"%s%s\n\n%s",
		resStatusLine,
		resHeader,
		resContent,
	)
	return response
}

// change time format UTC to GMT
func getGMTTimeStr(t time.Time) string {
	gmtTimeStr := strings.Replace(
		t.UTC().Format(time.RFC1123),
		"UTC",
		"GMT",
		-1,
	)
	return gmtTimeStr
}

// logger response info
func loggerResponseInfo(conn net.Conn, res *Response) {
	clientIp := conn.RemoteAddr().String()
	getPath := res.Request.PathInfo
	statusCode := res.StatusCode

	logger.Logger.Info(
		"client %s get %s status code %s",
		clientIp,
		getPath,
		statusCode,
	)
}

// start response
func StartResponse(conn net.Conn, reqChans chan *request.Request) {
	for request := range reqChans {
		res := GoResponse(request)
		conn.Write([]byte(res.generateResponse()))
		loggerResponseInfo(conn, res)
	}
}
