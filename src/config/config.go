/*
* go http server config file
*
 */

package config

import (
	"os"
	"path/filepath"
	"reflect"
	"strconv"

	"github.com/vaughan0/go-ini"
	"logger"
)

// load config meta
var configMeta = map[string]map[string]string{
	"Ip": {
		"section": "host",
		"field":   "ip",
		"type":    "string",
	},
	"Port": {
		"section": "host",
		"field":   "port",
		"type":    "string",
	},
	"StaticDir": {
		"section": "static",
		"field":   "dir",
		"type":    "string",
	},
	"LoggerLevel": {
		"section": "logging",
		"field":   "loglevel",
		"type":    "string",
	},
	"Timeout": {
		"section": "advance",
		"field":   "timeout",
		"type":    "int64",
	},
	"ReqChanCap": {
		"section": "advance",
		"field":   "reqchancap",
		"type":    "int64",
	},
}

type serverConfig struct {
	Ip          string
	Port        string
	StaticDir   string
	LoggerLevel string
	Timeout     int64
	ReqChanCap  int64
}

// load default static path
func (serConf *serverConfig) setDefault() {
	dir := getCurrDir()
	serConf.StaticDir = dir + "/www/"
}

// load ini config file
func (serConf *serverConfig) loadServerConfig() {
	reflect_config := reflect.ValueOf(SerConfig).Elem()
	configFile := getCurrDir() + "/config.ini"

	if iniFile, err := ini.LoadFile(configFile); err == nil {

		for fieldKey, fieldMeta := range configMeta {
			if setting, err := iniFile.Get(fieldMeta["section"], fieldMeta["field"]); err {

				switch fieldMeta["type"] {
				case "string":
					reflect_config.FieldByName(fieldKey).SetString(setting)
				case "int64":
					if cap, err := strconv.ParseInt(setting, 10, 64); err == nil {
						reflect_config.FieldByName(fieldKey).SetInt(cap)
					} else {
						logger.Logger.Warning(
							"load server config reqchancap failed %s can't convert to int, use default setting",
							setting,
						)
					}
				}
			}
		}
		logger.Logger.Info("found server config file, already load")
	} else {
		logger.Logger.Info("not found server config file, use default config")
	}
}

func getCurrDir() string {
	dir, _ := filepath.Abs(filepath.Dir(os.Args[0]))
	return dir
}

// init default server config
var SerConfig = &serverConfig{
	Ip:          "0.0.0.0",
	Port:        "8888",
	LoggerLevel: "INFO",
	ReqChanCap:  20,
	Timeout:     10,
}

func init() {
	SerConfig.setDefault()
	SerConfig.loadServerConfig()
	logger.Logger.Info("static dir %s", SerConfig.StaticDir)
}
