package main

import (
	"fmt"
	"net"
	"os"
	"time"

	"config"
	"logger"
	"request"
	"response"
)

type GoHttpServer struct {
	listener *net.TCPListener
}

func InitServer() *GoHttpServer {
	hostAddr := fmt.Sprintf(
		"%s:%s",
		config.SerConfig.Ip,
		config.SerConfig.Port,
	)
	tcpAddr, err := net.ResolveTCPAddr("tcp", hostAddr)
	checkError(err)
	logger.Logger.Info("Go Server start at %s", hostAddr)

	listener, err := net.ListenTCP("tcp", tcpAddr)
	checkError(err)

	server := &GoHttpServer{listener: listener}
	return server
}

func (ghs *GoHttpServer) handleClient(conn net.Conn) {
	reqChans := request.RequestsChans(conn)
	response.StartResponse(conn, reqChans)
}

func (ghs *GoHttpServer) ServerForever() {
	for {
		conn, err := ghs.listener.AcceptTCP()
		if err != nil {
			logger.Logger.Warning(
				"Accept Client connection error, error msg %s",
				err.Error(),
			)
			continue
		}
		timeout := time.Second * time.Duration(config.SerConfig.Timeout)
		conn.SetDeadline(time.Now().Add(timeout))
		go ghs.handleClient(conn)
	}
}

func checkError(err error) {
	if err != nil {
		logger.Logger.Fatalf("Network Error, Error msg %s", err.Error())
		os.Exit(1)
	}
}

func main() {
	logger.SetLogLevel(config.SerConfig.LoggerLevel)

	goHttpServer := InitServer()
	goHttpServer.ServerForever()
}
