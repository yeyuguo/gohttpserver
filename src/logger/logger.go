/*
* go http server logger
*
 */

package logger

import (
	"github.com/op/go-logging"
)

var Logger = logging.MustGetLogger("GoHttpServer")

func init() {
	format := logging.MustStringFormatter("%{level}-%{message}")
	logging.SetFormatter(format)
	logging.SetLevel(logging.INFO, "GoHttpServer")
}

// Set Logger Level
// level can be CRITICAL, ERROR, WARNING, NOTICE, INFO, DEBUG
func SetLogLevel(level string) {
	if loggingLevel, err := logging.LogLevel(level); err == nil {
		logging.SetLevel(loggingLevel, "GoHttpServer")
	} else {
		Logger.Warning("log level set failed %s is invalid log level, check the log level setting", level)
	}
}
